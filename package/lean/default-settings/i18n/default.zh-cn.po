msgid "Processor"
msgstr "处理器"

msgid "Architecture"
msgstr "架构"

msgid "CPU Temperature"
msgstr "CPU温度"

msgid "CPU Info"
msgstr "CPU信息"

msgid "CPU Model"
msgstr "处理器型号"

msgid "CPU frequency"
msgstr "CPU频率"

msgid "RAM frequency"
msgstr "RAM频率"

msgid "Flash Size"
msgstr "闪存大小"

msgid "Free Memory"
msgstr "释放内存"

msgid "RUNNING"
msgstr "运行中"

msgid "NOT RUNNING"
msgstr "未运行"

msgid "ZRam Settings"
msgstr "ZRam 设置"

msgid "ZRam Compression Algorithm"
msgstr "ZRAM压缩算法"

msgid "ZRam Compression Streams"
msgstr "ZRam 压缩数据流线程数"

msgid "ZRam Size"
msgstr "ZRam 大小"

msgid "Size of the ZRam device in megabytes"
msgstr "划分给ZRam 分区的内存大小（MB），推荐留空由系统自动管理"

msgid "Number of parallel threads used for compression"
msgstr "用于压缩内存数据的CPU并发线程数"

msgid "Swap"
msgstr "虚拟内存"

msgid "Force 40MHz mode"
msgstr "强制40MHz频宽"

msgid ""
"Always use 40MHz channels even if the secondary channel overlaps. Using this "
"option does not comply with IEEE 802.11n-2009!"
msgstr "强制启用40MHz频宽并忽略辅助信道重叠。此选项可能不兼容某些无线硬件导致无法启用!"

msgid "Disassociate On Low Acknowledgement"
msgstr "弱信号剔除"

msgid "Allow AP mode to disconnect STAs based on low ACK condition"
msgstr "允许AP模式基于低ACK条件判断剔除弱信号的客户端"

msgid "Disks"
msgstr "磁盘信息"

msgid "Disks Model"
msgstr "磁盘型号"

msgid "Serial number"
msgstr "磁盘序列号"

msgid "Firmware"
msgstr "固件版本"

msgid "Capacity"
msgstr "容量"

msgid "Sector size"
msgstr "扇区大小"

msgid "Temperature"
msgstr "温度"

msgid "Power state"
msgstr "电源状态"

msgid "Startup Delay"
msgstr "启动延迟"

msgid "MWAN Interfaces"
msgstr "MWAN接口信息"

msgid "untagged"
msgstr "无标记"

msgid "tagged"
msgstr "已标记"

msgid "Custom WEB UI directory"
msgstr "自定义Web UI目录"

msgid "Successful safe shutdown!"
msgstr "已经关机！"

msgid "Power Off"
msgstr "关机"

msgid "PowerOff the operating system of your device"
msgstr "安全关机，某些机器不能完全断电，关机后只能通过手工重新启动。"

msgid "Perform PowerOff"
msgstr "执行关机"

msgid "RAM Size"
msgstr "内存容量"

msgid "CPU usage"
msgstr "CPU使用率"

msgid "Unsupported"
msgstr "不支持"

msgid "CPU Model"
msgstr "CPU型号"

msgid "AriaNg"
msgstr "打开AriaNg的Web管理界面"

msgid "WebUI-Aria2"
msgstr "打开WebUI-Aria2的Web管理界面"

msgid ""
msgstr ""

msgid "YAAW"
msgstr "打开YAAW的Web管理界面"

msgid ""
msgstr ""

msgid ""
msgstr ""

msgid ""
msgstr ""

msgid ""
msgstr ""

msgid ""
msgstr ""

msgid ""
msgstr ""

msgid ""
msgstr ""
